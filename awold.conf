# This is a configuration file for AWOLD (Anti-WOL) - a remote WOL packet based
# daemon, which allows to suspend / shutdown / reboot computer based on
# received WOL packets while computer is on!
#
# This is intented to use with existing tools w/o any modifications to apps
# or tools used or modifications to standard WOL packets.
#
# To send the WOLs one can use OS utilities like wol, wakeonlan or phone /
# tablet or what have you, and remotely sleep / shutdown / reboot computer.

# Since this is running in local network or at least closely controlled
# network, supposedly there are no rogue packets going through it. This allows
# us to receive WOL packets in controlled fashion.
# Please configure, if needed, the options below.

# This defines maximum timeout daemon will listen to subsequent packets from
# first to last to determine whether the received packet count is actionable.
# If time differeence between first and next packet is larger, no action is
# taken.
# In short: one have to ensure that enough packets are sent within configured
# time to be considered actionable.
# Options (timeout in seconds, default 3):
#     secs: 3
#
AWOL_ACTION_TIMEOUT=3

# This option defines a timeout in seconds to prevent next action (sleep / 
# shutdown / reboot) right after one already has been performed. This option is
# here to avoid situations when user impatiently sends a lot of WOLs while
# one action is being performed (applies to sleep mostly).
# Another case when this is needed - if one tries to wake up computer with
# bursts of WOLs while using this utility, it may happen computer started up
# and WOL count somehow matched the received WOL burst count. In this case
# computer will suddenly sleep / shut down / restart right after startup.
# To prevent this, a timeout is needed!
# There should be no real life implications as people usually do not start
# the computer to be put to sleep or restart or shutdown (remotely) right after
# boot or sleep.
# Options (timeout in seconds, default 30):
#     secs: 30
#
AWOL_NO_ACTION_TIMEOUT=30

# This option controls how many WOLs recevied within defined time interval
# (AWOL_ACTION_TIMEOUT) correspond to action which needs to be performed.
# In short how many times one have to send in WOLs to be actionable for each
# of the supported actions.
# Negative values (-1) disable the feature.
# Example (default):
#     if one sends 3 WOL packets within 3 seconds, computer will be shut down.
# Options (WOL count, defaults):
#     sleep: 2
#     shutdown: 3
#     reboot: 4
#
AWOL_ACTION_SLEEP=2
AWOL_ACTION_SHUTDOWN=3
AWOL_ACTION_REBOOT=4
