# Anti-WOL daemon / service

</br>

## How it came to be

For various purposes I am using WOL to wake up my computer to do various tasks 
while outside home. It works well using ssh outside the home and at home I'm 
using my phone to wake up computer.

One day I was lying on my couch watching a movie but my computer was still on 
and my lazy buttocks did not want to get up and put computer to sleep :) So 
the idea was born to somehow put computer to sleep remotely.

</br>

## Design idea

I did not want to rely on some custom made thing, like a web-service or some 
other thing which requires additional application to accomplish the task. So I 
needed it to work with existing tool, that is the same existing software used 
to wake up computer using WOL.

So the idea was born to capture WOL requests (packets) while computer is on 
and act on the count of WOLs.

Other requirement was that it must be compatible with Linux and Windows as my 
sons (he uses Windows) buttocks is as lazy as mine :)

</br>

## How it works

AWOL works on WOL count while computer is on. For instance, if one sends two 
WOL requests to computer while it is on, the daemon gathers them, count them 
and acts on that.

There is a configuration file `/etc/awol/awold.conf` which explains the 
options. That configuration file acts as source of enviroment varibles for the 
daemon in Linux, but on Windows one can configure enviroment variables for 
those options.

The configuration file, of course, is technical Linux thing, I'll try to 
explain options in simple words below. In short, here is the brief description 
of options.

</br>

### Short usage summary

One has to have a phone app or some tool, to wake up computer. I'm using WOL 
app on my phone.

To wake up computer, one can open the app and click on "wake up" once.

To put the computer to sleep, one can use the same app, but click on "wake up" 
2 times within 3 seconds.

To shutdown computer, one can use the same app, but click on "wake up" 3 times 
within 3 seconds.

To reboot the computer, one can use the same app, but click on "wake up" 4 
times within 3 seconds.

</br>

More detailed description below is more technical, however for those who want 
to kwnow a bit more, it might be useful.

</br>

</br>

#### AWOL_ACTION_TIMEOUT - how long AWOL waits for WOL requests

To act on WOL count, we need a time interval for which the WOL packets are 
counted. The default is `3` seconds.

That means that one has to click 2 times on phone app (or what you) within 
3 seconds, to put computer to sleep. There are more options for shutdown and 
reboot, they are explained in sections below.

#### AWOL_NO_ACTION_TIMEOUT - a non-operation timeout

The option controls for how long after an action (for example, sleep) it 
will ignore WOL requests.

The option is needed in case one has strange WOL app which will send bursts 
of WOLs to make sure computer wakes up and that might iterfere with 
AWOL. Noone wants their computer be put to sleep right after it is woken up 
by accident. So there it is, for default `30` seconds and no WOLs will bother 
your computer.

#### AWOL_ACTION_SLEEP - WOL request count for sleep

This defines how many WOL requests accounts for sleep. Default: `2` 
While using default configuration one has to request WOL for 2 times within 
3 seconds and computer will go to sleep.

#### AWOL_ACTION_SHUTDOWN - WOL request count for shutdown

This defines how many WOL requests accounts for shutdown. Default: `3` 
While using default configuration one has to request WOL for 3 times within 
3 seconds and computer will go to sleep.

#### AWOL_ACTION_REBOOT - WOL request count for reboot

This defines how many WOL requests accounts for reboot. Default: `4`
While using default configuration one has to request WOL for 4 times within 
3 seconds and computer will go to reboot.

</br>

## Build

### Linux

On Linux it's simple. I use ArchLinux and I needed to install couple of 
things:

```sudo pacman -S gcc asio```

To compile, one needs `gcc` / `g++` and `asio` and to compile, just run:

```g++ -s -o awold awol.cpp```

### Windows

On Windows it's quite more involved, one needs Visual Studio, compiler and 
asio libraries installed.

I'll write a followup for this or ask my son to do it, he was the one who 
compiled this on Windows.

TBC

</br>

## Packages / releases

### Linux

I'll maintain the package for ArchLinux at least for my own needs, it's 
located here: [AWOL](https://aur.archlinux.org/packages/awol)

The package will provide a daemon which will run as soon as computer starts 
and will provide the functionality.

### Windows

For Windows I'll ask my son to provide and exe for this. As this needs to run 
in background one can use 
[NSSM](https://nssm.cc/) (the Non-Sucking Service Manager).

TBC
