#if defined(_WIN32) || defined(_WIN64)
// disable getenv and locatime warnings on windows
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream>
#include <cmath>
#include <asio.hpp>
#include <thread>
#include <mutex>
#if defined(__linux__)
#elif defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#include <iphlpapi.h>
#pragma comment(lib, "iphlpapi.lib")
#else
#error Unsupported operating system
#endif

// For development purposes (comment out when not testing)
//#define AWOL_DEV_MODE                    1

// Max adapters
#define AWOL_MAX_ADAPTERS               32
// Password offset
#define AWOL_PASSW_OFFSET              102
// Packet type validation
#define AWOL_MAGIC_PACKET_ERROR         -1
#define AWOL_MAGIC_PACKET_NORMAL         0
#define AWOL_MAGIC_PACKET_SLEEP          1
#define AWOL_MAGIC_PACKET_SHUTDOWN       2
#define AWOL_MAGIC_PACKET_RESTART        3

// Thread is executing
std::mutex _threadExecLock;
std::atomic<bool> _threadExecuting = false;

// Counts for actions
int _packetCountSleep    = 2;
int _packetCountPoweroff = 3;
int _packetCountReboot   = 4;

// #########################
// This code supports extension of passowrds (placed after 16x mac)
// however, unless there is a specific client build, noone can use it
// maybe in the future
// #########################

// Time ready for logging
auto logtime()
{
    // Vars
    char ftime[24];
    std::time_t t = std::time(nullptr);
    const std::tm* tm = std::localtime(&t);
    // Get and return time for logging
    std::strftime(ftime, sizeof(ftime), "[%Y.%m.%e %T] ", tm);
    return std::string(ftime);
}

// Load configration
void loadCofiguration(int* pActionTimeout, int* pPacketTimeout)
{
    // Vars
    char* option = NULL;
    // Configuration is loaded from environment variables
    option = std::getenv("AWOL_ACTION_TIMEOUT");
    // If defined
    if (option != NULL)
        if (atoi(option) > 0)
            *pPacketTimeout = std::stoi(option);
    // Configuration is loaded from environment variables
    option = std::getenv("AWOL_NO_ACTION_TIMEOUT");
    // If defined
    if (option != NULL)
        if (atoi(option) > 0)
            *pActionTimeout = std::stoi(option);
    // Configuration is loaded from environment variables
    option = std::getenv("AWOL_ACTION_SLEEP");
    // If defined
    if (option != NULL)
        if (atoi(option) != 0)
            _packetCountSleep = std::stoi(option);
    // Configuration is loaded from environment variables
    option = std::getenv("AWOL_ACTION_SHUTDOWN");
    // If defined
    if (option != NULL)
        if (atoi(option) != 0)
            _packetCountPoweroff = std::stoi(option);
    // Configuration is loaded from environment variables
    option = std::getenv("AWOL_ACTION_REBOOT");
    // If defined
    if (option != NULL)
        if (atoi(option) != 0)
            _packetCountReboot = std::stoi(option);
}

// Check if magic packet is valid
int isMagicPacketValid(const unsigned char* pOwnmac, const std::size_t pSizeo, const unsigned char* pBuffer, const std::size_t pSizeb)
{
    // Result
    int result = AWOL_MAGIC_PACKET_ERROR;

    // If not the right size
    if (!(pSizeb == AWOL_PASSW_OFFSET || pSizeb == AWOL_PASSW_OFFSET + 6)) {
        std::cout << logtime() << "check: received packet is not a magic packet" << std::endl;
        return result;
    }

    // Own mac
    std::array<unsigned char, 1024> expectedMAC;
    // "sleep" mac / pwd
    std::array<unsigned char, 6> expectedSleepMAC = {0x73, 0x6c, 0x65, 0x65, 0x70, 0xff};
    // "pwroff" mac / pwd
    std::array<unsigned char, 6> expectedShutdownMAC = {0x70, 0x77, 0x72, 0x6f, 0x66, 0x66};
    // "reboot" mac / pwd
    std::array<unsigned char, 6> expectedRestartMAC = {0x72, 0x65, 0x62, 0x6f, 0x6f, 0x74};
    // Received mac
    std::array<unsigned char, 1024> receivedMAC;
    // Received "sleep/sutdown"
    std::array<unsigned char, 6> receivedSleepShutdownMAC;

    // Fill in the packet
    for (auto i = 0; i < pSizeo; i++)
        expectedMAC[i] = pOwnmac[i];
    // Fill in the packet
    for (auto i = 0; i < pSizeb - 6; i++)
        receivedMAC[i] = pBuffer[6 + i];
    // Fill in the "sleep/shutdown" packet
    for (auto i = 0; i < 6; i++)
        receivedSleepShutdownMAC[i] = (pSizeb >= AWOL_PASSW_OFFSET + 6 ? pBuffer[AWOL_PASSW_OFFSET + i] : 0x00);

    std::cout << logtime() << "check: received magic packet for: ";
    for (auto i = 0; i < 6; ++i) {
        if (i > 0)
            printf(":");
        printf("%02x", (unsigned char) receivedMAC[i]);
    }
    std::cout << std::endl;

    // Check if this is magic packet (size only) and whether own mac match with what was received (start & middle)
    for(auto i = 0; i < pSizeo/6; i++) {
        // The mac address in magic packtet is repeated 16 times, lets check some random one
        int firstMac = 0, rndMac = std::rand() % 15;

        // Print out what we check
        std::cout << logtime() << "check: checking (" << i + 1 << " of " << pSizeo/6 << ", using " << firstMac + 1 << "/" << rndMac + 1  << " mac reps): ";
        for (auto j = 0; j < 6; ++j) {
            if (j > 0)
                printf(":");
            printf("%02x", (unsigned char) pOwnmac[i * 6 + j]);
        }
        std::cout << std::endl;

        // Check if packet acutally conforms to magic
        if (std::equal(receivedMAC.begin() + firstMac * 6, receivedMAC.begin() + (firstMac + 1) * 6, expectedMAC.begin() + (i * 6))
            && std::equal(receivedMAC.begin() + rndMac * 6, receivedMAC.begin() + (rndMac + 1) * 6, expectedMAC.begin() + (i * 6)))
        {
            // Check magic packet for "sleep/shutdown"
            if (pSizeb >= AWOL_PASSW_OFFSET + 6 && std::equal(receivedMAC.begin() + AWOL_PASSW_OFFSET, receivedMAC.begin() + AWOL_PASSW_OFFSET + 6, expectedSleepMAC.begin())) {
                result = AWOL_MAGIC_PACKET_SLEEP;
                std::cout << logtime() << "check: mac matches \"sleep\" packet" << std::endl;
            }
            else if (pSizeb >= AWOL_PASSW_OFFSET + 6 && std::equal(receivedMAC.begin() + AWOL_PASSW_OFFSET, receivedMAC.begin() + AWOL_PASSW_OFFSET + 6, expectedShutdownMAC.begin())) {
                result = AWOL_MAGIC_PACKET_SHUTDOWN;
                std::cout << logtime() << "check: mac matches \"shutdown\" packet" << std::endl;
            }
            else if (pSizeb>= AWOL_PASSW_OFFSET + 6 && std::equal(receivedMAC.begin() + AWOL_PASSW_OFFSET, receivedMAC.begin() + AWOL_PASSW_OFFSET + 6, expectedRestartMAC.begin())) {
                result = AWOL_MAGIC_PACKET_RESTART;
                std::cout << logtime() << "check: mac matches \"restart\" packet" << std::endl;
            } else {
                result = AWOL_MAGIC_PACKET_NORMAL;
                std::cout << logtime() << "check: mac matches" << std::endl;
            }
        } else {
            std::cout << logtime() << "check: does not match" << std::endl;
        }
        // Mac matches
        if (result != AWOL_MAGIC_PACKET_ERROR)
            break;
    }

    // Final result
    return (result);
}

// Perform action
void performAction(std::atomic<int>* pActionPcktCount, int* pSleepSecs, int* pNoActionSleepSecs, std::atomic<std::uint64_t>* pNoActionTime, std::atomic<std::uint64_t>* pActionTime)
{
    // Lock thread
    _threadExecLock.lock();
    _threadExecuting = true;

    // Reset pause time
    *pNoActionTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    std::uint64_t actualSleepMSecs = std::max({(*pSleepSecs * 1000 - (*pNoActionTime - *pActionTime)), (std::uint64_t)50});

    // Start exec
    std::cout << logtime() << "exec: prepare for action after " << round(actualSleepMSecs/100)/10 << " seconds..." << std::endl;

    // Sleep
    std::this_thread::sleep_for(std::chrono::milliseconds(actualSleepMSecs));

    // Perform platform-specific shutdown / suspend / restart command
    // Suspend
    if (*pActionPcktCount == _packetCountSleep) {
        std::cout << logtime() << "exec: going for sleep now..." << std::endl;
#if !defined(AWOL_DEV_MODE)
#if defined(__linux__)
        system("systemctl suspend");
#elif defined(_WIN32) || defined(_WIN64)
        system("rundll32 powrprof.dll,SetSuspendState 0,1,0");
#endif
#endif
    } else if (*pActionPcktCount == _packetCountPoweroff) {
        std::cout << logtime() << "exec: going for shutdown now..." << std::endl;
#if !defined(AWOL_DEV_MODE)
#if defined(__linux__)
        system("systemctl poweroff");
#elif defined(_WIN32) || defined(_WIN64)
        system("shutdown /s /t 0");
#endif
#endif
    } else if (*pActionPcktCount == _packetCountReboot) {
        std::cout << logtime() << "exec: going for restart now..." << std::endl;
#if !defined(AWOL_DEV_MODE)
#if defined(__linux__)
        system("systemctl reboot");
#elif defined(_WIN32) || defined(_WIN64)
        system("shutdown /r /t 0");
#endif
#endif
    } else {
        std::cout << logtime() << "exec: packet count not valid to execute anything..." << std::endl;
    }

    // Only if there was action
    if (*pActionPcktCount == _packetCountSleep || *pActionPcktCount == _packetCountPoweroff || *pActionPcktCount == _packetCountReboot) {
        std::cout << logtime() << "exec: action dispatched, inhibiting actions for ~ " << *pNoActionSleepSecs - *pSleepSecs << " seconds" << std::endl;
        // Sleep for non processing time
        std::this_thread::sleep_for(std::chrono::seconds(*pNoActionSleepSecs - *pSleepSecs));
    }

    // This allows discarding pending packets which might be accumulated while sleeping
    *pNoActionTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count()
        - std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::milliseconds((*pNoActionSleepSecs - *pSleepSecs - 1) * 1000)).count();

    // Reset packet count
    *pActionPcktCount = 0;
    std::cout << logtime() << "exec: execution finished" << std::endl;

    // Unlock thread
    _threadExecLock.unlock();
    _threadExecuting = false;
}

// Main procedure
int main()
{
    // Get own mac addresses
    int maccnt = 0;
    unsigned char pOwnmac[AWOL_MAX_ADAPTERS * 6];
#if defined(__linux__)
    struct ifreq ifr;
    struct ifconf ifc;
    char buf[1024];

    // Init random
    std::srand(std::time(nullptr));

    // Initialize socket
    int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

    // Get all interfaces
    ifc.ifc_len = sizeof(buf);
    ifc.ifc_buf = buf;
    if (ioctl(sock, SIOCGIFCONF, &ifc)) {
        std::cout << "Failed to get network adapter list" <<  std::endl;
        return 1;
    }

    // Structs for interfaces
    struct ifreq* it = ifc.ifc_req;
    const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));

    // Loop through devices, find non-lo interfaces and get mac addresses
    for (; it != end; ++it) {
        strcpy(ifr.ifr_name, it->ifr_name);
        // Get adapter info
        if (ioctl(sock, SIOCGIFFLAGS, &ifr) == 0) {
            // We do not need lo interfaces
            if (!(ifr.ifr_flags & IFF_LOOPBACK)) {
                // Get macaddress
                if (ioctl(sock, SIOCGIFHWADDR, &ifr) == 0) {
                    memcpy(&pOwnmac[maccnt * 6], ifr.ifr_hwaddr.sa_data, 6);
                    maccnt++;
                } else {
                    std::cout << "if (pAdapterInfo->OperStatus == IfOperStatusUp)Failed to get network adapter " << it->ifr_name << " macaddress" <<  std::endl;
                }
            }
        } else {
            std::cout << "Failed to get network adapter " << it->ifr_name << " information" <<  std::endl;
        }
    }
#elif defined(_WIN32) || defined(_WIN64)
    // Get adapter info
    ULONG outBufLen = 0;
    GetAdaptersAddresses(AF_UNSPEC, 0, NULL, NULL, &outBufLen);
    PIP_ADAPTER_ADDRESSES pAddresses = (PIP_ADAPTER_ADDRESSES)malloc(outBufLen);
    DWORD dwStatus = GetAdaptersAddresses(AF_UNSPEC, 0, NULL, pAddresses, &outBufLen);
    // Failure
    if (dwStatus != ERROR_SUCCESS) {
        // Free up memory
        free(pAddresses);
        std::cout << "Failed to get network adapter information" << std::endl;
        return 1;
    }

    // While there are more adapters
    while (pAddresses) {
        // Only if adapter is connected
        if (pAddresses->OperStatus == IfOperStatusUp && pAddresses->IfType != IF_TYPE_SOFTWARE_LOOPBACK) {
            // Assign address
            for (int i = 0; i < pAddresses->PhysicalAddressLength; i++) {
                pOwnmac[maccnt * 6 + i] = (unsigned char)pAddresses->PhysicalAddress[i];
            }
            maccnt++;
        }
        // Next adapter
        pAddresses = pAddresses->Next;
    }
    // Free up memory
    free(pAddresses);
#endif

    // Exit when no mac addresses are found, service needs to be restarted
    if (!(maccnt > 0)) {
        std::cout << "No devices found, is the network online?" << std::endl;
        return 1;
    }

    // Set up context
    asio::io_context ioContext;
    asio::ip::udp::socket socket(ioContext, asio::ip::udp::v4());
    asio::ip::udp::endpoint endpoint(asio::ip::udp::v4(), 9); // Bind to port 9

    // Enable socket option to allow broadcasting
    socket.set_option(asio::socket_base::broadcast(true));

    // Bind the socket to the endpoint
    socket.bind(endpoint);

    // Vars
    unsigned char buffer[102];
    int magicPacketCheckResult = 0;
    int actionTimeout = 30, packetTimeout = 3, minActionPacketCnt = 0;
    std::uint64_t ttimemsecs = 0;
    std::thread performActionThread;
    std::atomic<int> packetCnt = 0;
    std::atomic<std::uint64_t> lastActionTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    std::atomic<std::uint64_t> lastPacketTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

    // Load configconfig
    loadCofiguration(&actionTimeout, &packetTimeout);

    // Determnine minimum packet count for action
    minActionPacketCnt = std::min({_packetCountSleep, _packetCountPoweroff, _packetCountReboot});

    std::cout << "Rules:" << std::endl;
    std::cout << "  for suspend / shutdown / restart to work, at least " << minActionPacketCnt << " magic packets must be received within " << packetTimeout << " seconds interval" << std::endl;
    std::cout << "  suspend / shutdown / restart will be inhibited for " << actionTimeout << " seconds after startup or last performed action" << std::endl;
    std::cout << "  options: suspend WOL count: " << _packetCountSleep << ", shutdown WOL count: " << _packetCountPoweroff  << ", reboot WOL count: " << _packetCountReboot << std::endl;

    std::cout << "Devices:" << std::endl;
    for (auto i = 0; i < maccnt; ++i) {
        std::cout << "  device (" << i + 1 << " of " << maccnt << "): ";
        for (auto j = 0; j < 6; ++j) {
            if (j > 0)
                printf(":");
            printf("%02x", (unsigned char) pOwnmac[6 * i + j]);
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    std::cout << logtime() << "Listening for WOL magic packet on port 9..." << std::endl;

    // Infinite loop
    while (true) {
        // Listen
        std::size_t receivedBytes;

        // Receive magic packet
        receivedBytes = socket.receive(asio::buffer(buffer));
        std::cout << logtime() << "daemon: WOL magic packet arrived..." << std::endl;

        // Check if magic packet is for us and it is actually valid
        if ((magicPacketCheckResult = isMagicPacketValid(pOwnmac, maccnt * 6, buffer, receivedBytes)) > AWOL_MAGIC_PACKET_ERROR) {
            // Actual actionable packet - we process sleep/shutdown/reboot immediately
            if (magicPacketCheckResult == AWOL_MAGIC_PACKET_SLEEP) {
                lastPacketTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
                packetCnt = _packetCountSleep;
            } else if (magicPacketCheckResult == AWOL_MAGIC_PACKET_SHUTDOWN) {
                lastPacketTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
                packetCnt = _packetCountPoweroff;
            } else if (magicPacketCheckResult == AWOL_MAGIC_PACKET_RESTART) {
                lastPacketTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
                packetCnt = _packetCountReboot;
            } else {
                packetCnt++;
            }

            // Decode stuff for user
            if (packetCnt >= minActionPacketCnt && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count() - lastPacketTime) / 1000 <= packetTimeout) {
                if (packetCnt == _packetCountSleep) {
                    std::cout << logtime() << "daemon: current AWOL action - sleep" << std::endl;
                } else if (packetCnt == _packetCountPoweroff) {
                    std::cout << logtime() << "daemon: current AWOL action - poweroff" << std::endl;
                } else if (packetCnt == _packetCountReboot) {
                    std::cout << logtime() << "daemon: current AWOL action - reboot" << std::endl;
                } else {
                    std::cout << logtime() << "daemon: current AWOL action - none" << std::endl;
                }
            }

            // Do not allow suspend for 30 secs after initiating it
            ttimemsecs = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count() - lastActionTime;
            // Discard all while thread is executing
            if (_threadExecuting) {
                std::cout << logtime() << "daemon: action is already in progress..." <<  std::endl;
                continue;
            // Inhibint magic packets
            } else if (ttimemsecs / 1000 < actionTimeout) {
                std::cout << logtime() << "daemon: not processing packets for " << actionTimeout << " secs since startup or last action, resuming in " << actionTimeout - ttimemsecs / 1000 << " seconds" << std::endl;
                packetCnt = 0;
                continue;
            // Start timer on first packet
            } else if (packetCnt == 1) {
                lastPacketTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
            // If more than 1 packet did not arrive in time provided
            } else if (packetCnt >= minActionPacketCnt && (std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count() - lastPacketTime) / 1000 > packetTimeout) {
                std::cout << logtime() << "daemon: subsequent magic packets did not arrive in " << packetTimeout << " seconds, resetting wait period" << std::endl;
                lastPacketTime = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
                packetCnt = minActionPacketCnt - 1;
                continue;
            // start accounting time
            } else if (packetCnt >= minActionPacketCnt) {
                // If we can't get a lock, thread is executing and we do nothing
                if (_threadExecLock.try_lock()) {
                    // Thread is not executing, so we start up execution thread
                    _threadExecuting = false;
                    performActionThread = std::thread {performAction, &packetCnt, &packetTimeout, &actionTimeout, &lastActionTime, &lastPacketTime};
                    // Leave thread alone to do its thing whislt gathering more packets (in case user decides to perform other action)
                    performActionThread.detach();
                    // Unlock the thread lock from here and wait for thread to start up
                    _threadExecLock.unlock();
                    while(!_threadExecuting) {std::this_thread::sleep_for(std::chrono::milliseconds(1));}
                }
            }
        } else {
            std::cout << logtime() << "daemon: received packet does not match the structure or it's not for this computer" << std::endl;
            packetCnt = 0;
        }
    }
    // Done
    return 0;
}
